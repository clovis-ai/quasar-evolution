/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scene.map;

/**
 * The types of ground
 * @author CLOVIS
 */
public enum GroundType {
    WATER,
    DIRT,
    GRASS,
    GRAVEL, // near cold water, or in hot mountains
    SAND,
    ROCK
}
