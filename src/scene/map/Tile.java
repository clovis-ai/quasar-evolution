/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scene.map;

import static processing.core.PApplet.pow;
import quasarevolution.Screen;
import utilities.simpledata.Color;

/**
 *
 * @author CLOVIS
 */
public class Tile {
    
    private Color mainColour;
    
    private final int coordX, coordY;
    
    private final int resolution;
    private float elevation;
    private float groundHeight;
    private float[][] elevationMap;
    private float[][] shadeMap;
    private Color[][] colourMap;
    private GroundType[][] groundType;
    private float moisture;
    private float[][] moistureMap;
    private float temperature;
    private float[][] temperatureMap;
    
    public Tile(int resolution, long[] seeds, long[] bio_seeds, long[] moist_seeds, long[] temp_seeds, int x, int y){
        coordX = x;
        coordY = y;
        this.resolution = resolution;
        elevationMap = perlinGenerator(seeds, 5, 0.4f, 100);
        moistureMap = perlinGenerator(moist_seeds, 3, 0.9f, 50);
        temperatureMap = perlinGenerator(temp_seeds, 5, 0.1f, 600);
        shadeMap = new float[resolution][resolution];
        groundHeight = simplePerlinGenerator(bio_seeds, 5, 0.1f, 50) * 2f;
        //groundHeight = 0.8f;
        elevation = 0;
        for(int ny = 0; ny < resolution; ny++){
            for(int nx = 0; nx < resolution; nx++){
                elevationMap[nx][ny] *= groundHeight * 18;
                temperatureMap[nx][ny] *= 70;
                temperatureMap[nx][ny] -= 20;
                moistureMap[nx][ny] *= 30;
                if(elevationMap[nx][ny] < Map.SEA_LEVEL * 1.4){
                    moistureMap[nx][ny] += (Map.SEA_LEVEL * 1.4 - elevationMap[nx][ny]) * 20;
                }
                if(moistureMap[nx][ny] > 100)  moistureMap[nx][ny] = 100;
                if(moistureMap[nx][ny] < 0)    moistureMap[nx][ny] = 0;
                elevation += elevationMap[nx][ny];
            }
        }
        temperature = temperatureMap[0][0];
        moisture = moistureMap[0][0];
        elevation /= resolution * resolution;
        
        groundType = new GroundType[resolution][resolution];
        for(int ny = 0; ny < resolution; ny++){
            for(int nx = 0; nx < resolution; nx++){
                if(elevationMap[nx][ny] <= Map.SEA_LEVEL){
                    groundType[nx][ny] = GroundType.WATER;
                }else if(elevationMap[nx][ny] <= Map.SEA_LEVEL*1.2){
                    if(temperature > 10){
                        groundType[nx][ny] = GroundType.SAND;
                    }else{
                        groundType[nx][ny] = GroundType.GRAVEL;
                    }
                }else if(elevationMap[nx][ny] < Map.LAST_PLANTS_LEVEL){
                    if(moistureMap[nx][ny] > 25 && moistureMap[nx][ny] < 90){
                        groundType[nx][ny] = GroundType.GRASS;
                    }else if(moistureMap[nx][ny] > 24){
                        groundType[nx][ny] = GroundType.DIRT;
                    }else{
                        groundType[nx][ny] = GroundType.SAND;
                    }
                }else{
                    groundType[nx][ny] = GroundType.ROCK;
                }
            }
        }
        
        refreshColours();
        //System.out.println(terrainData());
    }
    
    public void refreshColours(){
        if(elevation < 7){
            mainColour = new Color((short) 0, (short) 0, (short) 255);
        }else if(elevation < 7.5){
            mainColour = new Color((short) 220, (short) 220, (short) 255);
        }else if(elevation < 8){
            mainColour = new Color((short) 255, (short) 255, (short) 50);
        }else if(elevation < 12){
            mainColour = new Color((short) 0, (short) 255, (short) 0);
        }else if(elevation < 14){
            mainColour = new Color((short) 125);
        }else{
            mainColour = new Color((short) 255);
        }
        Screen.screen.fill(mainColour.getRedValue(), mainColour.getGreenValue(), mainColour.getBlueValue());
        Screen.screen.rect(coordX*resolution, coordY*resolution, resolution, resolution);
        Screen.screen.fill(255 - moisture * 2.55f);
        Screen.screen.rect(coordX*resolution+Screen.screen.width/2, coordY*resolution, resolution, resolution);
        Screen.screen.fill(Tile.groundTypeColor(groundType[0][0]).getRedValue(), groundTypeColor(groundType[0][0]).getGreenValue(), groundTypeColor(groundType[0][0]).getBlueValue());
        Screen.screen.rect(coordX*resolution, coordY*resolution+Screen.screen.height/2, resolution, resolution);
        Screen.screen.fill((temperature + 20) * 2.55f);
        Screen.screen.rect(coordX*resolution+Screen.screen.width/2, coordY*resolution+Screen.screen.height/2, resolution, resolution);
    }
    
    /**
     * Creates a Perlin-generated noise map (values between 0..1).
     * @param seeds The seeds of each octaves
     * @param lacunarity Defines how fast octaves shrink
     * @param persistence Defines how fast octaves loose importance (0..1)
     * @param zoom Default = 1, to zoom in use values greater than 1
     * @return 
     */
    private float[][] perlinGenerator(long[] seeds, float lacunarity, float persistence, float zoom){
        float[][] noiseMap = new float[resolution][resolution];
        int cX = coordX * resolution;
        int cY = coordY * resolution;
        
        for(int s = 0; s < seeds.length; s++){
            long seed = seeds[s];
            Screen.screen.noiseSeed(seed);
            float frequency = pow(lacunarity, s);
            float amplitude = pow(persistence, s);
            for(int y = 0; y < resolution; y++){
                for(int x = 0; x < resolution; x++){
                    noiseMap[x][y] += Screen.screen.noise(frequency * (x + cX)/zoom, frequency * (y + cY)/zoom) * amplitude;
                }
            }
        }
        /*for(int y = 0; y < resolution; y++){
            for(int x = 0; x < resolution; x++){
                Screen.screen.fill(noiseMap[x][y] * 255);
                Screen.screen.rect((x + cX) * 5, (y + cY) * 5, 5, 5);
            }
        }*/
        return noiseMap;
    }
    
    /**
     * Creates a Perlin-generated noise value (between 0..1).
     * @param seeds The seeds of each octaves
     * @param lacunarity Defines how fast the octaves shrink
     * @param persistence Defines how fast the octaves loose importance (0..1)
     * @param zoom Default = 1, to zoom in use values greater than 1
     * @return 
     */
    private float simplePerlinGenerator(long[] seeds, float lacunarity, float persistence, float zoom){
        float noiseValue = 0;
        int cX = coordX * resolution;
        int cY = coordY * resolution;
        
        for(int s = 0, n = seeds.length; s < n; s++){
            long seed = seeds[s];
            Screen.screen.noiseSeed(seed);
            float frequency = pow(lacunarity, s);
            float amplitude = pow(persistence, s);
            noiseValue += Screen.screen.noise(frequency * (cX)/zoom, frequency * (cY)/zoom) * amplitude;
        }
        
        return noiseValue;
    }
    
    public String terrainData(){
        return "X:"+coordX+"\tY:"+coordY+"\tMoisture:"+moisture+"\tTemperature:"+temperature+"\tElevation:"+elevation;
    }
    
    public static final Color groundTypeColor(GroundType t){
        if(t == null){ System.err.println("Ground type shouldn't be null");}
        //System.out.println("GroundType : "+t);
        Color c;
        switch(t){
            case WATER:
                c = new Color(0, 0, 255); break;
            case SAND:
                c = new Color(255, 255, 0); break;
            case DIRT:
                c = new Color(139, 69, 19); break;
            case GRASS:
                c = new Color(0, 255, 0); break;
            case GRAVEL:
                c = new Color(100); break;
            case ROCK:
                c = new Color(75); break;
            default:
                c = new Color(0);
        }
        return c;
    }
}
