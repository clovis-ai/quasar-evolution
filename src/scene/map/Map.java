/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scene.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import quasarevolution.Screen;
import utilities.simpledata.Couple;

/**
 *
 * @author CLOVIS
 */
public class Map {
    public HashMap<Couple<Integer>, Tile> map;
    private long[] seeds;
    private long[] moistureSeeds;
    private long[] temperatureSeeds;
    private long[] biomeSeeds;
    
    public static final float SEA_LEVEL = 7;
    public static final float LAST_PLANTS_LEVEL = 13;
    
    public Map(){
        this(new long[]{(long)Screen.screen.random(Long.MAX_VALUE), (long)Screen.screen.random(Long.MAX_VALUE), (long)Screen.screen.random(Long.MAX_VALUE)},
             new long[]{(long)Screen.screen.random(Long.MAX_VALUE), (long)Screen.screen.random(Long.MAX_VALUE)},
             new long[]{(long)Screen.screen.random(Long.MAX_VALUE), (long)Screen.screen.random(Long.MAX_VALUE)},
             new long[]{(long)Screen.screen.random(Long.MAX_VALUE), (long)Screen.screen.random(Long.MAX_VALUE)});
    }
    
    public Map(long[] s, long[] bs, long[] ms, long[] ts){
        seeds = s;
        moistureSeeds = ms;
        temperatureSeeds = ts;
        biomeSeeds = bs;
        System.out.println("Seeds :\t"+seeds[0]+"\t"+seeds[1]);
        System.out.println("Moist :\t"+moistureSeeds[0]+"\t"+moistureSeeds[1]);
        System.out.println("Tempe :\t"+temperatureSeeds[0]+"\t"+temperatureSeeds[1]);
        toLoad = new LinkedList<Couple<Integer>>();
        map = new HashMap<Couple<Integer>, Tile>();
    }
    
    
    
    
    Queue<Couple<Integer>> toLoad;
    
    public void load(){
        Couple<Integer> current = toLoad.poll();
        if(current == null)             return;
        if(map.containsKey(current))    return;
        map.put(current, new Tile(1, seeds, biomeSeeds, moistureSeeds, temperatureSeeds, current.X(), current.Y()));
    }
    
    public void addToLoadList(int x, int y){
        toLoad.add(new Couple(x, y));
    }
}
