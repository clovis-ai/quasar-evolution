/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quasarevolution;

import java.io.File;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import processing.core.PApplet;
import scene.map.Map;
import scene.map.Tile;

/**
 *
 * @author CLOVIS
 */
public class Screen extends PApplet {
    
    private static boolean gameOn;
    private final String DATA_PATH = getDataFolderPath();
    public static Screen screen;
    private String getDataFolderPath(){ try {return new File(new File(Screen.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile().getAbsolutePath() + File.separator + "data").getAbsolutePath();} catch (URISyntaxException ex) {Logger.getLogger(Screen.class.getName()).log(Level.SEVERE, null, ex);return "";}}
    
    private Map map;
    
    /**
     * This function is used by Processing - don't call it.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        PApplet.main("quasarevolution.Screen");
    }
    
    /**
     * Specify the settings for Processing
     */
    @Override
    public void settings(){
        size(800, 640);
        screen = this;
    }
    
    /**
     * Specify the setup for Processing
     */
    @Override
    public void setup(){
        noStroke();
        frameRate(500);
        //map = new Map(new long[]{542, 485},
        //              new long[]{485, 564},
        //              new long[]{456, 156},
        //              new long[]{154, 225});
        map = new Map();
        background(125);
    }
    int nx = 0, ny = 0;
    /**
     * The animation thread
     */
    @Override
    public void draw(){
        surface.setTitle("QUASAR Evolution Pre-Alpha 1.0.3 | FPS:" + (int)frameRate + " | Tiles:"+map.map.size());
        for(int y = 0; y < 20; y++){
            for(int x = 0; x < 20; x++){
                map.addToLoadList(nx + x, ny + y);
                map.load();
            }
        }
        map.load();
        nx += 20;
        if(nx + 10 > width/2){
            nx = 0;
            ny += 20;
        }
        if(ny + 10 > height/2){
            fill(255);
            text("Terrain", 10, 15);
            text("Moisture", 10 + width/2, 15);
            text("Ground type", 10, 15 + height/2);
            text("Temperature", 10 + width/2, 15 + height/2);
            noLoop();
        }
    }
    
    /**
     * The loading thread
     */
    public void load(){
        
    }
    
}

