/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.simpledata;

import utilities.checkedNumbers.CheckedDouble;
import utilities.checkedNumbers.CheckedShort;

/**
 * It's a color :)
 * @author CLOVIS
 * @version 2.0
 */
public class Color {
    private CheckedShort red, green, blue;
    private CheckedDouble alpha;
    
    /** Minimum allowed value for colors */
    public static final short COLOR_MIN_VALUE = (short) 0;
    /** Maximum allowed value for colors */
    public static final short COLOR_MAX_VALUE = (short) 255;
    
    /**
     * Creates a color using its RGB value and a alpha value
     * @param r Red value
     * @param g Green value
     * @param b Blue value
     * @param a Alpha value (transparency, 0..1)
     * @see #COLOR_MAX_VALUE
     * @see #COLOR_MIN_VALUE
     */
    public Color(short r, short g, short b, double a){
        red = new CheckedShort(r, COLOR_MIN_VALUE, COLOR_MAX_VALUE);
        green = new CheckedShort(g, COLOR_MIN_VALUE, COLOR_MAX_VALUE);
        blue = new CheckedShort(b, COLOR_MIN_VALUE, COLOR_MAX_VALUE);
        alpha = new CheckedDouble(a, 0, 1);
    }
    
    /**
     * Creates a color using its RGB value and a alpha value
     * @param r Red value
     * @param g Green value
     * @param b Blue value
     * @param a Alpha value (transparency, 0..1)
     * @see #COLOR_MAX_VALUE
     * @see #COLOR_MIN_VALUE
     */
    public Color(int r, int g, int b, double a){
        this((short) r, (short) g, (short) b, a);
    }
    
    /**
     * Creates a color using its RGB value
     * @param r Red value
     * @param g Green value
     * @param b Blue valuen
     * @see #COLOR_MAX_VALUE
     * @see #COLOR_MIN_VALUE
     */
    public Color(short r, short g, short b){
        this(r, g, b, 0);
    }
    
    /**
     * Creates a color using its RGB value
     * @param r Red value
     * @param g Green value
     * @param b Blue valuen
     * @see #COLOR_MAX_VALUE
     * @see #COLOR_MIN_VALUE
     */
    public Color(int r, int g, int b){
        this((short) r, (short) g, (short) b, 0);
    }
    
    /**
     * Creates a shade of grey with transparency
     * @param grey Shade of grey
     * @param alpha Transparency (0..1)
     * @see #COLOR_MAX_VALUE
     * @see #COLOR_MIN_VALUE
     */
    public Color(short grey, double alpha){
        this(grey, grey, grey, alpha);
    }
    
    /**
     * Creates a shade of grey with transparency
     * @param grey Shade of grey
     * @param alpha Transparency (0..1)
     * @see #COLOR_MAX_VALUE
     * @see #COLOR_MIN_VALUE
     */
    public Color(int grey, double alpha){
        this((short) grey, (short) grey, (short) grey, alpha);
    }
    
    /**
     * Creates a shade of grey
     * @param grey Shade of grey
     * @see #COLOR_MAX_VALUE
     * @see #COLOR_MIN_VALUE
     */
    public Color(short grey){
        this(grey, grey, grey, 0);
    }
    
    /**
     * Creates a shade of grey
     * @param grey Shade of grey
     * @see #COLOR_MAX_VALUE
     * @see #COLOR_MIN_VALUE
     */
    public Color(int grey){
        this((short) grey, (short) grey, (short) grey, 0);
    }
    
    // ********************************** BASIC ********************************
    
    /**
     * Use this function to set or increment the red value.
     * @return The red value
     */
    public CheckedShort getRed(){
        return red;
    }
    
    /**
     * Shortcut to get the red value as a short.
     * @return The red value
     */
    public short getRedValue(){
        return red.getValue();
    }
    
    /**
     * Use this function to set or increment the green value.
     * @return The green value
     */
    public CheckedShort getGreen(){
        return green;
    }
    
    /**
     * Shortcut to get the green value as a short.
     * @return The green value
     */
    public short getGreenValue(){
        return green.getValue();
    }
    
    /**
     * Use this function to set or increment the blue value.
     * @return The blue value
     */
    public CheckedShort getBlue(){
        return blue;
    }
    
    /**
     * Shortcut to get the blue value as a short.
     * @return The blue value
     */
    public short getBlueValue(){
        return blue.getValue();
    }
    
    /**
     * Use this function to set or increment the alpha value.
     * @return The alpha value
     */
    public CheckedDouble getAlpha(){
        return alpha;
    }
    
    /**
     * Shortcut to get the alpha value as a short.
     * @return The alpha value
     */
    public double getAlphaValue(){
        return alpha.getValue();
    }
    
    // ********************************** MEDIUM *******************************
    
    /**
     * The maximum value of the channels (excluding alpha)
     * @return the brightness
     */
    public short maximum(){
        short r = red.getValue(), g = green.getValue(), b = blue.getValue();
        if(r >= g && r >= b){
            return r;
        }else if(g >= r && g >= b){
            return g;
        }else{
            return b;
        }
    }
    
    /**
     * The minimum value of the channels (excluding alpha)
     * @return the hue
     */
    public short minimum(){
        short r = red.getValue(), g = green.getValue(), b = blue.getValue();
        if(r <= g && r <= b){
            return r;
        }else if(g <= r && g <= b){
            return g;
        }else{
            return b;
        }
    }
    
    /**
     * The average value of the channels (excluding alpha)
     * @return the saturation
     */
    public short greyScale(){
        short r = red.getValue(), g = green.getValue(), b = blue.getValue();
        return (short) ((r + g + b)/3f);
    }
}