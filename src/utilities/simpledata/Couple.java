/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.simpledata;

import java.util.Objects;

/**
 * Stores two values of the same type and performs basic operations on them.
 * @author CLOVIS
 * @param <Type> The type
 * @version 0.1
 */
public class Couple<Type> {
    private Type x;
    private Type y;
    
    /**
     * Creates a new Couple of the two objects.
     * @param X The first object (X coordinate)
     * @param Y The second object (Y coordinate)
     */
    public Couple(Type X, Type Y){
        x = X; y = Y;
    }
    
    /**
     * Gets the first value of the Couple
     * @return the X coordinate
     */
    public Type X(){
        return x;
    }
    
    /**
     * Gets the second value of the Couple
     * @return the Y coordinate
     */
    public Type Y(){
        return y;
    }
    
    /**
     * Sets the first value of the Couple
     * @param nX the new value (X coordinate)
     */
    public void setX(Type nX){
        x = nX;
    }
    
    /**
     * Sets the second value of the Couple
     * @param nY the new value (Y coordinate)
     */
    public void setY(Type nY){
        y = nY;
    }
    
    /**
     * Creates a hashcode for this object.
     * @return The hashcode
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.x);
        hash = 23 * hash + Objects.hashCode(this.y);
        return hash;
    }

    /**
     * Evaluates the equality between this object and an other.
     * @param obj The other object
     * @return <i>true</i> if both objects are equal, <i>false</i> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null)                        return false;
        if (getClass() != obj.getClass())       return false;
        if (this == obj)                        return true;
        
        final Couple<?> other = (Couple<?>) obj;
        if (!Objects.equals(this.x, other.x))   return false;
        if (!Objects.equals(this.y, other.y))   return false;
        
        return true;
    }
}
