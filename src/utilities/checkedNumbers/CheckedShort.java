/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.checkedNumbers;

/**
 * A short which value is between the bounds you want. Note : two CheckedShort of the same value but different MINIMUM or MAXIMUM are considered identical.
 * @author CLOVIS
 */
public class CheckedShort {
    private short value;
    private final short MINIMUM, MAXIMUM;
    
    /**
     * Creates a checked number and initializes it
     * @param v The initial value
     * @param min The minimum value
     * @param max The maximum value
     */
    public CheckedShort(short v, short min, short max){
        MINIMUM = min;
        MAXIMUM = max;
        set(v);
    }
    
    /**
     * Creates a checked number and initializes it with only a min or a max.
     * @param v The initial value
     * @param type Wether it's a minimum or a maximum
     * @param minmax The minimum or maximum
     */
    public CheckedShort(short v, BoundType type, short minmax){
        if(type == BoundType.MINIMUM){
            MINIMUM = minmax;
            MAXIMUM = Short.MAX_VALUE;
        }else{
            MINIMUM = Short.MIN_VALUE;
            MAXIMUM = minmax;
        }
        set(v);
    }
    
    /**
     * Get the value of this number
     * @return the value
     */
    public short getValue(){
        return value;
    }
    
    /**
     * Sets the value. It will be checked for boundaries.
     * @param n The new value
     */
    public void set(short n){
        value = (n > MINIMUM) ? (n < MAXIMUM) ? n : MAXIMUM : MINIMUM;
    }
    
    /**
     * Increases the value (use negative numbers for decrease). The result will be checked for boundaries.
     * @param n How much you'd like to increase it
     */
    public void add(short n){
        set(n);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (this == obj){
            return true;
        }
        final CheckedShort other = (CheckedShort) obj;
        if (this.value != other.value) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.value;
        return hash;
    }
    
}
