/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities.checkedNumbers;

/**
 * A double which value is between the bounds you want. Note : two CheckedDouble of the same value but different MINIMUM or MAXIMUM are considered identical.
 * @author CLOVIS
 */
public class CheckedDouble {
    private double value;
    private final double MINIMUM, MAXIMUM;
    
    /**
     * Creates a checked number and initializes it
     * @param v The initial value
     * @param min The minimum value
     * @param max The maximum value
     */
    public CheckedDouble(double v, double min, double max){
        MINIMUM = min;
        MAXIMUM = max;
        set(v);
    }
    
    /**
     * Creates a checked number and initializes it with only a min or a max.
     * @param v The initial value
     * @param type Wether it's a minimum or a maximum
     * @param minmax The minimum or maximum
     */
    public CheckedDouble(double v, BoundType type, double minmax){
        if(type == BoundType.MINIMUM){
            MINIMUM = minmax;
            MAXIMUM = Double.MAX_VALUE;
        }else{
            MINIMUM = Double.MIN_VALUE;
            MAXIMUM = minmax;
        }
        set(v);
    }
    
    /**
     * Get the value of this number
     * @return the value
     */
    public double getValue(){
        return value;
    }
    
    /**
     * Sets the value. It will be checked for boundaries.
     * @param n The new value
     */
    public void set(double n){
        value = (n > MINIMUM) ? (n < MAXIMUM) ? n : MAXIMUM : MINIMUM;
    }
    
    /**
     * Increases the value (use negative numbers for decrease). The result will be checked for boundaries.
     * @param n How much you'd like to increase it
     */
    public void add(double n){
        set(n);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (this == obj){
            return true;
        }
        final CheckedDouble other = (CheckedDouble) obj;
        if (this.value != other.value) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + (int) (Double.doubleToLongBits(this.value) ^ (Double.doubleToLongBits(this.value) >>> 32));
        return hash;
    }
    
}
